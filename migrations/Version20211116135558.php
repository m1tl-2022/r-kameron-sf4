<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211116135558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE room_category');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE room_category (room_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_A6AAD90554177093 (room_id), INDEX IDX_A6AAD90512469DE2 (category_id), PRIMARY KEY(room_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE room_category ADD CONSTRAINT FK_A6AAD90512469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE room_category ADD CONSTRAINT FK_A6AAD90554177093 FOREIGN KEY (room_id) REFERENCES room (id) ON DELETE CASCADE');
    }
}
